import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Details from './pages/Details';
import Home from './pages/Home';
import Sales from "./pages/Sales";
import Brands from "./pages/Brands";
import Stores from "./pages/Stores";
import Popup from "./pages/Popup";

import NotFound from './pages/NotFound';

function App() {

    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Home} />
                <Route path="/brands" exact component={Brands} />
                <Route path="/sales" exact component={Sales} />
                <Route path="/stores" exact component={Stores} />
                <Route path="/pop" exact component={Popup} />
                <Route path="/details/:id" exact component={Details} />
                <Route component={NotFound} />
            </Switch>
        </BrowserRouter>
    )
}

export default App;