import React from 'react';
import { Link } from 'react-router-dom';
import uuid from 'react-uuid';
import JsonFromSQL from '../assets/db.json';

export default class Brands extends React.Component {

    toysTable = JsonFromSQL[0].toys; // Table "toys"
    brandsTable = JsonFromSQL[0].brands; // Table "brands"
    tmp = []

    state = {
        toyByBrand : [],
        toyBrand : []
   }

    componentDidMount() {

        this.FK_toyByBrand();
        this.populateBrandSelect();
    }

    populateBrandSelect( id = "#toyByBrand-select" ) {
        
        let optionSelect = "";

        this.brandsTable.forEach( (toyByBrand) => { 

            optionSelect = document.createElement("option");
            optionSelect.textContent = toyByBrand.name;
            optionSelect.value = toyByBrand.id;
            document.querySelector(id).appendChild(optionSelect);
            
        } );
    }

    changeBrand(e) {

        this.setState( {
            toyByBrand : this.toysTable
                .filter( toy => toy.brand_id === parseInt( e.target.value ) ) 
        } );  
    }

    FK_toyByBrand() {

        this.tmp = [];

        this.toysTable.forEach(toy => {
            
            this.tmp.push( {
                "toy_id" : toy.id,
                "brand_id" : toy.brand_id    
            });
        });
   
        this.setState( { toyByBrand: this.tmp } );
    }

    render() {

        return (
            <>
                <Link to="/">Home</Link>&nbsp;
                <Link to="/sales">Sales</Link>&nbsp;
                <Link to="/stores">Stores</Link><br></br><br></br>
                
                <select name="brands" id="toyByBrand-select" onChange={this.changeBrand.bind(this)}>
                    <option value="">Choisir une marque</option>
                </select>
                
                {                            
                    this.state.toyByBrand.map((toy) => {

                        return toy.image !== undefined ?

                            <div key={uuid()}>
                                <br></br>{toy.name} <br></br>
                                <img src={require(`../img/${toy.image}`).default} alt=""/>                        
                            </div>
                            
                            : null
                        }
                    )
                }
            </>
        );
    }
}
