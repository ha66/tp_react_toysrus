import React from 'react';
import { Link } from 'react-router-dom';
import uuid from 'react-uuid';
import JsonFromSQL from '../assets/db.json';

export default class Sales extends React.Component {

    state = {

        TopSalesResults : 0, // Nombre de résultats à afficher dans le top des ventes
        TopXSales : [] // Tableau devant recevoir les objets "toy" du top 3 des ventes (pour le rendu)
    }

    oldTopX = this.state.TopSalesResults;
    toysTable = JsonFromSQL[0].toys; // Table "toys"
    toysQuantity = this.toysTable.length; // Nombre total de jouets
    brandsTable = JsonFromSQL[0].brands; // Table "brands"
    salesTable = JsonFromSQL[0].sales; // Table "sales"
    soldToyQuantity = 0; // Total des ventes pour un jouet

    sales = []; // Tableau devant recevoir les "toy_id" ainsi que le total des ventes de chaque jouet

    tmp = [];
    colorPrice = "green"; // css
    colorName = "brown"; // css
    
    constructor(props) {

        super(props);

        this.state = {
            TopSalesResults : 3, // Nombre de résultats à afficher dans le top des ventes
            TopXSales : [] // Tableau devant recevoir les objets "toy" du top 3 des ventes (pour le rendu)
        }
    }

    componentDidMount() {

        this.FK_toySales();
        this.topX_Sales();        
    }


    FK_toySales() {

        for ( let i = 0; i < this.toysQuantity; i++ ) {
            
            this.salesTable

                .filter( sale => sale.toy_id === i )
                .filter( sale => this.soldToyQuantity += sale.quantity ); 
            
            this.sales.push( {
                 
                "toy_id" : i,
                "soldToyQuantity" : this.soldToyQuantity
            } );

            this.soldToyQuantity = 0; // Réinitialisation de soldToyQuantity à chaque tour de boucle
        }       

        this.topX_Sales();
    }

    topX_Sales() {

        this.tmp = [];

        this.sales

            .sort( ( current, next ) => next.soldToyQuantity - current.soldToyQuantity ) 
            .splice( this.state.TopSalesResults, this.toysQuantity - this.state.TopSalesResults );
            
        this.sales.forEach( toy => { 
            this.tmp.push ( this.toysTable[ toy.toy_id - 1 ] ) 
        } );

        this.setState( { TopXSales : this.tmp } );
    }

    render() {

        return (
            <> &nbsp;
                <Link to="/">Home</Link>&nbsp;
                <Link to="/brands">Brands</Link>&nbsp;
                <Link to="/stores">Stores</Link><br></br><br></br> 
                <h1>&nbsp;&nbsp;Top {this.state.TopSalesResults} des ventes</h1><br></br><br></br> 
                {
                    this.state.TopXSales.map((topToy) => {

                        const { name, price, image } = topToy

                        return (
                            <div key={ uuid() }>
                                
                                <h3>{name}</h3>
                                <img src={require(`../img/${image}`).default} alt=""/>
                                <h3>{price}</h3>
                                <hr/>
                                
                            </div>
                            )
                        }
                    )
                }
            </>
        );
    }

}

