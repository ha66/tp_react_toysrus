

import React, { useEffect } from 'react';
import JsonFromSQL from "../assets/db.json";

const Ventes = () => {

    const JsonDB = JsonFromSQL[0].sales;
    
    let salesByToy = [];
    let soldToyQuantity = 0;

    useEffect(() => {
        
        for ( let i = 1; i <= JsonFromSQL[0].toys.length; i++ ) {

            JsonDB.filter( sale => sale.toy_id === i )
                .filter( toy => soldToyQuantity += toy.quantity !== undefined ? toy.quantity : null);

            salesByToy.push( {
                "toy_id" : i,
                "saleQuantity" : soldToyQuantity
            } );

            soldToyQuantity = 0;
        }
        
        salesByToy.sort( (a, b) => b.saleQuantity - a.saleQuantity );
        console.log( salesByToy );
        
    }, []);
    
    return (
        <>
       
        </>
    )
}

export default Ventes;