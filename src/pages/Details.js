import React from 'react';
import { Link } from 'react-router-dom';
import JsonFromSQL from "../assets/db.json";

export default class Details extends React.Component {

    state = {
        totalToyQuantityInAllStores : []   
    }

    toysTable = JsonFromSQL[0].toys;
    stockTable = JsonFromSQL[0].stock;
    storesTable = JsonFromSQL[0].stores;
    toyQuantity = 0;
    tmp = [];

    storedToyQuantity = [];

    componentDidMount() {

        this.totalToyStock();
        this.eachStoreStock();
        this.populateDomElementStoreSelectObject();
    }

    totalToyStock() {

        for ( let i = 1; i <= this.toysTable.length; i++ ) {

            this.stockTable

                .filter( stock => stock.toy_id === i )
                .filter( toy => this.toyQuantity += toy.quantity )
                
            this.tmp.push( this.toyQuantity );    
            this.toyQuantity = 0;
        }

        this.setState( { totalToyQuantityInAllStores : this.tmp } );
    }

    eachStoreStock( toyId = this.props.location.id ) {

        this.stockTable

            .filter( stock => stock.toy_id  === toyId )
            .filter( store => this.storedToyQuantity.push( store.quantity ) );
    }

    populateDomElementStoreSelectObject( htmlElementId = "#store-select" ) {
        
        let domElementStoreSelectObject = "";

        this.storesTable.forEach( store  => { 

            domElementStoreSelectObject = document.createElement( "option" );
            domElementStoreSelectObject.value = store.id;
            domElementStoreSelectObject.textContent = store.name + " ( " + this.storedToyQuantity[store.id-1] + " )";
            document.querySelector( htmlElementId ).appendChild( domElementStoreSelectObject );
        } );
    }

    changeStore(e) {
        console.log(e.target.value)
    }

    render() {

        const { name, price, id, img, desc } = this.props.location;

        return (
            <>  
                <Link to="/">Home</Link><br></br><br></br>
                <div><b>{name}</b></div>
                <div>Prix : <b>{price} €</b></div>
                <div>
                    Quantité totale : <b>{this.state.totalToyQuantityInAllStores[id-1]}</b>
                    &nbsp;
                    <select name="stores" id="store-select" onChange={this.changeStore.bind(this)}>
                        <option value="">Quel Magasin ?</option>
                    </select>
                </div>
                <div>
                    <img src={require(`../img/${img}`).default} alt=""/>
                </div>
                <div dangerouslySetInnerHTML={{__html:desc}}></div>
            </>
        );
    }
}