import React from 'react';
import { Link } from 'react-router-dom';
import uuid from 'react-uuid';
import JsonFromSQL from "../assets/db.json";

export default class Home extends React.Component {

    state = {
        toysList : []
    }

    toysTable = JsonFromSQL[0].toys;
    storesTable = JsonFromSQL[0].stores;
    index = 1;

    componentDidMount() {
        this.setState ( { toysList : this.toysTable } )    
    }

    priceSort(e) {

        parseInt(e.target.value) === 1 

            ? this.toysTable.sort( ( current, next ) => current.price - next.price )
            : this.toysTable.sort( ( current, next ) => next.price - current.price )

        this.index = 1;
        this.setState ( { toysList : this.toysTable } )  
    }


    render() {
        
        return (
            <>
                <div className = "accueil" style={{ backgroundImage: 'url( "../img/background2.png" )', backgroundRepeat: 'no-repeat' }}>
                <div className = "accueil2" style={{ backgroundImage: 'url( "../img/background1.png" )', backgroundRepeat: 'repeat' }}></div> 
                    <div style={{marginLeft: '250px', width: '400px', border: 'solid 1px red'}}>
                    <Link to = "/sales">Sales</Link> <Link to = "/brands">Brands</Link> <Link to = "/stores">Stores</Link>
                    &nbsp;&nbsp;             
                    <select name="sort" id="price-sort-select" onChange={ this.priceSort.bind(this) }>
                        <option value="">Trier</option>
                        <option value="1">Prix croissant</option>
                        <option value="2">Prix décroissant</option>
                    </select>
                    <br/><br/>
                    {   
                        this.state.toysList.map( ( thisToy ) => {
                            
                            const { name, desc, price, image } = thisToy;
                            
                            return (

                                <div key = { uuid() }>
                                    <h3>{ name }</h3>
                                    <Link to = { {
                                        pathname: `details/${ this.index }`,
                                        name: name,
                                        desc: desc,
                                        price: price,
                                        id: this.index++,
                                        img: image
                                        } }><img src = { require(`../img/${ image }`).default } alt = ""/>
                                    </Link>
                                    <h3>{ price } €</h3>
                                    <hr/>
                                </div>
                                
                                )
                            }
                        )
                    }
                    </div>
                </div>
            </>
        )
    }

}