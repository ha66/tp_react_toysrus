import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import uuid from 'react-uuid';
import JsonFromSQL from "../assets/db.json";

export default class Home extends Component {

    toysTable = JsonFromSQL[0].toys;
    storesTable = JsonFromSQL[0].stores;
    colorPrice = "green";
    colorName = "brown";

    componentDidMount() {

        this.populateStoreSelect();
    }

    populateStoreSelect( storeSelector = "#store-select" ) {
        
        let storeSelection = "";

        this.storesTable.forEach( store  => { 

            storeSelection = document.createElement("option");
            storeSelection.value = store.id;
            storeSelection.textContent = store.name;
            document.querySelector(storeSelector).appendChild(storeSelection);
        } );
    }

    changeStore(event) {

        //let index = event.target.value ;
        let e = document.getElementById("store-select");
        console.log(e.options[e.selectedIndex].text);
    }

    render() {
        return (
            <>
                <Link to="/">Home</Link>&nbsp;
                <Link to="/sales">Sales</Link>&nbsp;
                <Link to="/brands">Brands</Link>
                <br></br><br></br>
                <select name="stores" id="store-select" onChange={this.changeStore}>
                    <option value="">Quelle Magasin ?</option>
                </select>
                
                {
                    this.toysTable.map((thisToy) => {
                        return (
                            <div key={uuid()}>
                                <img src={require(`../img/${thisToy.image}`).default} alt=""/>
                                <h3 style={{color:this.colorName, backgroundColor:"#eee"}}>{thisToy.name}</h3>
                                {/* <div dangerouslySetInnerHTML={{__html:thisToy.description}} style={{color:"", backgroundColor:"#eee"}}></div> */}
                                <h3 style={{color:this.colorPrice}}>{thisToy.price} €</h3>
                            </div>
                            )
                        }
                    )
                }
            </>
        )
    }
}